class Code

  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow,
  }
  attr_reader :pegs

  def self.parse(str)
    pegs = str.split("").map do |el|
      raise "error" unless PEGS.has_key?(el.upcase)
      PEGS[el.upcase]
    end
    Code.new(pegs)
  end

  def self.random
    pegs = []
    4.times do
      pegs << PEGS.values.shuffle
    end
    Code.new(pegs)
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def [](i)
    pegs[i]
  end

  def exact_matches(other_code)
    count = 0
    pegs.each_with_index do |color, idx|
      count += 1 if color == other_code[idx]
    end
    count
  end

  def near_matches(other_code)
    match = []
    pegs.each do |color|
      match << color if other_code.pegs.include?(color)
    end
    match.uniq.count - self.exact_matches(other_code)
  end

  def ==(other_code)
    self.exact_matches(other_code) == 4
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "Please a guess!"
    Code.parse(gets.chomp)
  end

  def display_matches(guess)
    exacts = @secret_code.exact_matches(guess)
    nears = @secret_code.near_matches(guess)
    puts "You have #{exacts} exact matches and #{nears} near matches"
  end

end
